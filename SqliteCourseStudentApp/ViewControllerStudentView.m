/*
 *		The MIT License (MIT)
 *
 *
 *		Copyright (c) 2015 Ian Hattendorf
 *
 * 		Permission is hereby granted, free of charge, to any person obtaining a copy
 *		of this software and associated documentation files (the "Software"), to deal
 *		in the Software without restriction, including without limitation the rights
 *		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *		copies of the Software, and to permit persons to whom the Software is
 *		furnished to do so, subject to the following conditions:
 *
 *		The above copyright notice and this permission notice shall be included in
 *		all copies or substantial portions of the Software.
 *
 *		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *		THE SOFTWARE.
 *
 *		@author Ian Hattendorf mailto:Ian.Hattendorf@asu.edu
 *
 * 		@version April 20, 2015
 */

#import "ViewControllerStudentView.h"
#import "TableViewControllerStudent.h"
#import "StudentManager.h"
#import "Student.h"
#import "CourseManager.h"
#import "Course.h"

@interface ViewControllerStudentView ()
@property (weak, nonatomic) IBOutlet UITextField *TFName;
@property (weak, nonatomic) IBOutlet UITextField *TFEmail;
@property (weak, nonatomic) IBOutlet UITextField *TFMajor;
@property (weak, nonatomic) IBOutlet UITextField *TFCourseDrop;
@property (weak, nonatomic) IBOutlet UITextField *TFCourseAdd;

@property (strong, nonatomic) UIPickerView *pickerDrop;
@property (strong, nonatomic) UIPickerView *pickerAdd;

@property (strong, nonatomic) CourseManager *courseManager;
@end

@implementation ViewControllerStudentView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.courseManager = [CourseManager new];
    
    self.TFName.enabled = NO;
    self.TFEmail.enabled = NO;
    self.TFMajor.enabled = NO;
    
    self.pickerDrop = [UIPickerView new];
    [self.pickerDrop setDelegate:self];
    [self.pickerDrop setDataSource:self];
    self.pickerAdd = [UIPickerView new];
    [self.pickerAdd setDelegate:self];
    [self.pickerAdd setDataSource:self];
    
    [self.TFCourseDrop setInputView:self.pickerDrop];
    [self.TFCourseAdd setInputView:self.pickerAdd];

    [self displayStudent:self.student];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)removeStudent:(id)sender {
    [self.studentManager removeStudent:self.student];
    [self.parent reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dropCourse:(id)sender {
    NSString *title = self.TFCourseDrop.text;
    Course *course = [self.courseManager findCourseByTitle:title];
    [self.studentManager removeStudent:self.student fromCourse:course];
    self.TFCourseDrop.text = @"";
    [self.pickerDrop reloadAllComponents];
    [self.pickerAdd reloadAllComponents];
    [self.parent reloadData];
}

- (IBAction)addCourse:(id)sender {
    NSString *title = self.TFCourseAdd.text;
    Course *course = [self.courseManager findCourseByTitle:title];
    [self.studentManager addStudent:self.student toCourse:course];
    self.TFCourseAdd.text = @"";
    [self.pickerDrop reloadAllComponents];
    [self.pickerAdd reloadAllComponents];
    [self.parent reloadData];
}

- (void)displayStudent:(Student *)student {
    self.TFName.text = student.name;
    self.TFEmail.text = student.email;
    self.TFMajor.text = student.major;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSArray *courses = [self.courseManager findAllByStudent:self.student];
    
    if (pickerView == self.pickerDrop) {
        Course *course = courses[row];
        [self.TFCourseDrop resignFirstResponder];
        self.TFCourseDrop.text = course.title;
    } else if (pickerView == self.pickerAdd) {
        NSArray *coursesNotIn = [self addObjects];
        [self.TFCourseAdd resignFirstResponder];
        self.TFCourseAdd.text = coursesNotIn[row];
    }
}

// messy
- (NSArray *)addObjects {
    NSArray *courses = [self.courseManager findAllByStudent:self.student];
    NSMutableArray *coursesNames = [NSMutableArray new];
    for (Course *course in courses)
        [coursesNames addObject:course.title];
    NSArray *allCourses = [self.courseManager findAll];
    NSMutableArray *allCoursesNames = [NSMutableArray new];
    for (Course *course in allCourses)
        [allCoursesNames addObject:course.title];
    NSMutableSet *allSet = [NSMutableSet setWithArray:allCoursesNames];
    NSSet *studentsSet = [NSSet setWithArray:coursesNames];
    [allSet minusSet:studentsSet];
    NSArray *coursesNotIn = [allSet allObjects];
    return coursesNotIn;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title = @"unknown";
    if (pickerView == self.pickerAdd) {
        NSArray *coursesNotIn = [self addObjects];
        if (row < coursesNotIn.count) {
            title = coursesNotIn[row];
        }
    } else if (pickerView == self.pickerDrop) {
        NSArray *courses = [self.courseManager findAllByStudent:self.student];
        if (row < courses.count) {
            Course *course = courses[row];
            title = course.title;
        }
    }
    return title;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == self.pickerAdd) {
        return [self addObjects].count;
    } else if (pickerView == self.pickerDrop) {
        NSArray *courses = [self.courseManager findAllByStudent:self.student];
        return courses.count;
    }
    return 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
