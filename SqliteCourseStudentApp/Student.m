/*
 *		The MIT License (MIT)
 *
 *
 *		Copyright (c) 2015 Ian Hattendorf
 *
 * 		Permission is hereby granted, free of charge, to any person obtaining a copy
 *		of this software and associated documentation files (the "Software"), to deal
 *		in the Software without restriction, including without limitation the rights
 *		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *		copies of the Software, and to permit persons to whom the Software is
 *		furnished to do so, subject to the following conditions:
 *
 *		The above copyright notice and this permission notice shall be included in
 *		all copies or substantial portions of the Software.
 *
 *		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *		THE SOFTWARE.
 *
 *		@author Ian Hattendorf mailto:Ian.Hattendorf@asu.edu
 *
 * 		@version April 20, 2015
 */

#import "Student.h"

@implementation Student

- (id)initWithName:(NSString *)name email:(NSString *)email major:(NSString *)major {
    if (self = [super init]) {
        self.name = name;
        self.email = email;
        self.major = major;
    }
    return self;
}

- (id)initFromArray:(NSArray *)array {
    if (self = [super init]) {
        self.studentId = [array[0] integerValue];
        self.name = array[1];
        if (array.count > 2)
            self.email = array[2];
        if (array.count > 3)
            self.major = array[3];
    }
    return self;
}

- (NSString *)toString {
    return [NSString stringWithFormat:@"Name: %@, Email: %@, Major: %@", self.name, self.email, self.major];
}

@end
