/*
 *		The MIT License (MIT)
 *
 *
 *		Copyright (c) 2015 Ian Hattendorf
 *
 * 		Permission is hereby granted, free of charge, to any person obtaining a copy
 *		of this software and associated documentation files (the "Software"), to deal
 *		in the Software without restriction, including without limitation the rights
 *		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *		copies of the Software, and to permit persons to whom the Software is
 *		furnished to do so, subject to the following conditions:
 *
 *		The above copyright notice and this permission notice shall be included in
 *		all copies or substantial portions of the Software.
 *
 *		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *		THE SOFTWARE.
 *
 *		@author Ian Hattendorf mailto:Ian.Hattendorf@asu.edu
 *
 * 		@version April 20, 2015
 */

#import "StudentManager.h"
#import "Student.h"
#import "Course.h"
#import "CourseDbManager.h"

@interface StudentManager ()
@property (strong, nonatomic) CourseDbManager *courseDb;
@end

@implementation StudentManager

-(id)init {
    if ( self = [super init] ) {
        self.courseDb = [[CourseDbManager alloc] initDatabaseName:@"CourseStudentDatabase"];
    }
    return self;
}

- (void)addStudent:(Student *)student {
    NSArray *params = @[student.name, student.email, student.major];
    BOOL success = [self.courseDb executeUpdate:@"insert into students (name, email, major) values (?, ?, ?)" withParams:params];
    if (success)
        student.studentId = self.courseDb.lastInsertID;
    else
        NSLog(@"failed to add course");
}

// student must already exist in database
- (void)addStudent:(Student *)student toCourse:(Course *)course {
    NSArray *params = @[[NSNumber numberWithInteger:course.courseId], [NSNumber numberWithInteger:student.studentId]];
    BOOL success = [self.courseDb executeUpdate:@"insert into courses_students (course_id, student_id) values (?, ?)" withParams:params];
    if (!success)
        NSLog(@"failed to add course");
}

- (void)removeStudent:(Student *)student {
    NSArray *params = @[[NSNumber numberWithInteger:student.studentId]];
    BOOL success = [self.courseDb executeUpdate:@"delete from students where id = ?" withParams:params];
    if (!success)
        NSLog(@"failed to remove student");
}

- (void)removeStudent:(Student *)student fromCourse:(Course *)course {
    NSArray *params = @[[NSNumber numberWithInteger:student.studentId], [NSNumber numberWithInteger:course.courseId]];
    BOOL success = [self.courseDb executeUpdate:@"delete from courses_students where student_id = ? and course_id = ?" withParams:params];
    if (!success)
        NSLog(@"failed to remove student from course");
}

- (NSArray *)findAll {
    NSArray *queryResult = [self.courseDb executeQuery:@"select id, name, email, major from students"];
    NSMutableArray *students = [NSMutableArray array];
    for (NSArray *studentArray in queryResult) {
        Student *student = [[Student alloc] initFromArray:studentArray];
        [students addObject:student];
    }
    return students;
}

- (NSArray *)findAllInCourse:(Course *)course {
    NSString *query = [NSString stringWithFormat:@"select s.id, s.name, s.email, s.major from courses_students cs inner join students s on cs.student_id = s.id where cs.course_id = %ld", course.courseId];
    NSArray *queryResult = [self.courseDb executeQuery:query];
    NSMutableArray *students = [NSMutableArray array];
    for (NSArray *studentArray in queryResult) {
        Student *student = [[Student alloc] initFromArray:studentArray];
        [students addObject:student];
    }
    return students;
}

- (Student *)findStudentById:(NSInteger)studentId {
    NSString *query = [NSString stringWithFormat:@"select id, name, email, major from students where id = %ld", (long)studentId];
    NSArray *queryResult = [self.courseDb executeQuery:query];
    Student *student = [[Student alloc] initFromArray:queryResult[0]];
    return student;
}

- (Student *)findStudentByName:(NSString *)name {
    NSString *query = [NSString stringWithFormat:@"select id, name, email, major from students where name = '%@'", name];
    NSArray *queryResult = [self.courseDb executeQuery:query];
    Student *student = [[Student alloc] initFromArray:queryResult[0]];
    return student;
}

- (NSInteger)studentCount {
    NSArray *queryResult = [self.courseDb executeQuery:@"select count(*) from students"];
    NSInteger count = [queryResult[0][0] intValue];
    return count;
}

- (NSInteger)studentCountInCourse:(Course *)course {
    NSString *query = [NSString stringWithFormat:@"select count(*) from courses_students where course_id = %ld", course.courseId];
    NSArray *queryResult = [self.courseDb executeQuery:query];
    NSInteger count = [queryResult[0][0] intValue];
    return count;
}

@end
