/*
 *		The MIT License (MIT)
 *
 *
 *		Copyright (c) 2015 Ian Hattendorf
 *
 * 		Permission is hereby granted, free of charge, to any person obtaining a copy
 *		of this software and associated documentation files (the "Software"), to deal
 *		in the Software without restriction, including without limitation the rights
 *		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *		copies of the Software, and to permit persons to whom the Software is
 *		furnished to do so, subject to the following conditions:
 *
 *		The above copyright notice and this permission notice shall be included in
 *		all copies or substantial portions of the Software.
 *
 *		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *		THE SOFTWARE.
 *
 *		@author Ian Hattendorf mailto:Ian.Hattendorf@asu.edu
 *
 * 		@version April 20, 2015
 */

#import "ViewControllerStudentAdd.h"
#import "TableViewControllerStudent.h"
#import "TableViewControllerContacts.h"
#import "StudentManager.h"
#import "Student.h"
#import "Course.h"

@interface ViewControllerStudentAdd ()
@property (weak, nonatomic) IBOutlet UITextField *TFName;
@property (weak, nonatomic) IBOutlet UITextField *TFEmail;
@property (weak, nonatomic) IBOutlet UITextField *TFMajor;
@end

@implementation ViewControllerStudentAdd

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.TFName setDelegate:self];
    [self.TFEmail setDelegate:self];
    [self.TFMajor setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)saveStudent:(id)sender {
    NSString *name = self.TFName.text;
    NSString *email = self.TFEmail.text;
    NSString *major = self.TFMajor.text;
    Student *student = [[Student alloc] initWithName:name email:email major:major];
    [self.studentManager addStudent:student];
    [self.studentManager addStudent:student toCourse:self.course];
    NSLog(@"Added student: %@ to course: %@", student.toString, self.course.toString);
    [self.parent reloadData];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadContactName:(NSString *)name email:(NSString *)email {
    self.TFName.text = name;
    self.TFEmail.text = email;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"studentAddToContacts"]) {
        TableViewControllerContacts *destController = segue.destinationViewController;
        destController.parent = self;
    }
}


@end
