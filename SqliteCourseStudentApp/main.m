//
//  main.m
//  SqliteCourseStudentApp
//
//  Created by Ian Hattendorf on 4/16/15.
//  Copyright (c) 2015 Ian Hattendorf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
