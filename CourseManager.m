/*
 *		The MIT License (MIT)
 *
 *
 *		Copyright (c) 2015 Ian Hattendorf
 *
 * 		Permission is hereby granted, free of charge, to any person obtaining a copy
 *		of this software and associated documentation files (the "Software"), to deal
 *		in the Software without restriction, including without limitation the rights
 *		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *		copies of the Software, and to permit persons to whom the Software is
 *		furnished to do so, subject to the following conditions:
 *
 *		The above copyright notice and this permission notice shall be included in
 *		all copies or substantial portions of the Software.
 *
 *		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *		THE SOFTWARE.
 *
 *		@author Ian Hattendorf mailto:Ian.Hattendorf@asu.edu
 *
 * 		@version April 20, 2015
 */

#import "CourseManager.h"
#import "Course.h"
#import "CourseDbManager.h"
#import "Student.h"

@interface CourseManager ()
@property (strong, nonatomic) CourseDbManager *courseDb;
@end

@implementation CourseManager

-(id)init {
    if ( self = [super init] ) {
        self.courseDb = [[CourseDbManager alloc] initDatabaseName:@"CourseStudentDatabase"];
    }
    return self;
}

- (void)addCourse:(Course *)course {
    NSArray *params = @[course.title, course.instructor];
    BOOL success = [self.courseDb executeUpdate:@"insert into courses (title, instructor) values (?, ?)" withParams:params];
    if (success)
        course.courseId = self.courseDb.lastInsertID;
    else
        NSLog(@"failed to add course");
}

- (void)removeCourse:(Course *)course {
    NSArray *params = @[[NSNumber numberWithInteger:course.courseId]];
    BOOL success = [self.courseDb executeUpdate:@"delete from courses where id = ?" withParams:params];
    if (!success)
        NSLog(@"failed to remove course");
}

- (NSArray *)findAll {
    NSArray *queryResult = [self.courseDb executeQuery:@"select id, title, instructor from courses"];
    NSMutableArray *courses = [NSMutableArray array];
    for (NSArray *courseArray in queryResult) {
        Course *course = [[Course alloc] initFromArray:courseArray];
        [courses addObject:course];
    }
    return courses;
}

- (NSArray *)findAllByStudent:(Student *)student {
    NSString *query = [NSString stringWithFormat:@"select c.id, c.title, c.instructor from courses_students cs inner join courses c on cs.course_id = c.id where cs.student_id = %ld", student.studentId];
    NSArray *queryResult = [self.courseDb executeQuery:query];
    NSMutableArray *courses = [NSMutableArray array];
    for (NSArray *courseArray in queryResult) {
        Course *course = [[Course alloc] initFromArray:courseArray];
        [courses addObject:course];
    }
    return courses;
}

- (Course *)findCourseById:(NSInteger *)courseId {
    NSString *query = [NSString stringWithFormat:@"select id, title, instructor from courses where id = %ld", (long)courseId];
    NSArray *queryResult = [self.courseDb executeQuery:query];
    Course *course = [[Course alloc] initFromArray:queryResult[0]];
    return course;
}

- (Course *)findCourseByTitle:(NSString *)title {
    NSString *query = [NSString stringWithFormat:@"select id, title, instructor from courses where title = '%@'", title];
    NSArray *queryResult = [self.courseDb executeQuery:query];
    Course *course = [[Course alloc] initFromArray:queryResult[0]];
    return course;
}

- (NSInteger)courseCount {
    NSArray *queryResult = [self.courseDb executeQuery:@"select count(*) from courses"];
    NSInteger count = [queryResult[0][0] intValue];
    return count;
}

@end
